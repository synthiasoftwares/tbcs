<?php
/**
 * ark Admin Panel for Codeigniter 
 * Author: Abhishek R. Kaushik
 * downloaded from http://devzone.co.in
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    public function index() {
        if($_REQUEST['action']=='check')
            $this->getCheck();
	else if($_REQUEST['action']=='subproname')
            $this->getSubproname();
	else if($_REQUEST['action']=='proman')
            $this->getProman();
	else if($_REQUEST['action']=='addsubpro')
            $this->getAddsubpro();
	else if($_REQUEST['action']=='addpro')
            $this->getAddpro();
	else if($_REQUEST['action']=='editpro')
            $this->getEditpro();
	else if($_REQUEST['action']=='editsubpro')
            $this->getEditsubpro();
	else if($_REQUEST['action']=='uploadfile')
            $this->getUploadfile();
	else if($_REQUEST['action']=='single_insert')
            $this->getSingleinsert();
	else if($_REQUEST['action']=='bulk_insert')
            $this->getBulkinsert();
        else
            $this->get404();
    }


	public function getCheck(){		
			$query=$this->input->post('string');
			$spid=$this->input->post('subproname');
			$db=$this->input->post('db');
			$pid=$_REQUEST['proname'];


					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/search/?pid=".$pid."&spid=".$spid."&query=".urlencode($query)."&start=0&end=50&db=".$db;
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);

					$rmsg = $res['Response Message'];
					if ( $rmsg=='String not Matched')
					{
						$resHTML = '<div class="alert alert-success">
  <strong>This String is Unique. Would you like to save it?click on insert button to save it.</strong></div>';
					}
					else if ($rmsg=='Success')
					{
					$x=$res['Response Data'];
					//print_r($x);
					$resHTML='<div><center><h2 class="success text-success">Matched Strings</h2></center><table class="table table-bordered table-hover"><thead> <tr><th>S. No.</th><th>String</td><th>Percentage</td><tr></thead><tbody>';
					//echo json_encode($x['res']);
					$i=1;
					foreach($x as $data)
					{  $temp=(array)$data;
						if ($temp['perc']>75)
							$pbar = 'progress-bar-danger';
						else
							$pbar = 'progress-bar-warning';
						
						$resHTML.='<tr><td>'.$i.'</td><td>'.$temp['stri'].'</td><td><div class="progress">
  <div class="progress-bar '.$pbar.'" role="progressbar" aria-valuenow="70"
  aria-valuemin="0" aria-valuemax="'. $temp['perc'].'" style="width:'. $temp['perc'].'%">'. floor($temp['perc']).'% Matched</div>
</div></td></tr>';
						$i++;
					}
					$resHTML.=' </tbody></table> </div><div></div>';
					
					curl_close($curl);
					}
					else
					{
						$resHTML = '<div class="alert alert-danger">
  <strong>No string to match!</strong> Please input a String.
</div>';
					}
					echo $resHTML;
	}


	public function getSubproname(){
				if($_REQUEST['proname']!="Select Project")
				{
					$pid=$_REQUEST['proname'];
					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/subprojectlist/?pid=".$pid;
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);
					$rmsg = $res['subproject'];
					$resHTML='<select class="col-sm-5 pull-right" id="subproname" name="subproname">
					<option>Select Wave</option>';
					foreach ($rmsg as $p)
					{
					$resHTML.= '<option value="'.$p.'">'.$p.'</option>';
					}
					$resHTML.='</select>';
					echo $resHTML;
				}
				else 
					echo "";
	}


	public function getProman(){
				if($_REQUEST['proname']!="Select Existing Project:")
				{
					$pid=$_REQUEST['proname'];
					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/subprojectlist/?pid=".urlencode($pid);
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);
					$rmsg = $res['subproject'];
					if(empty($rmsg)){
					$resHTML='<br><span class="alert alert-danger">
  <strong>No Wave created!</strong> Please create a new wave.</span>';
					echo $resHTML;}
					else{
					$resHTML='<table class="table table-bordered"><thead><tr><th>S. No.</th><th>Wave Name</th><th>Edit Wave Name</th><th>Delete Wave?</th></tr></thead><tbody>';
					$i=1;
						foreach ($rmsg as $p)
						{
						$resHTML.= '<tr><td>'.$i++.'</td><td>'.$p.'</td>
<td class="text-info"><a href="'. base_url().'admin/proman/editsubpro?proname='.$pid.'&subproname='.$p.'"><center><button type="button" class="btn btn-info btn-xs editpro" id="'. $p .'"><i class="fa fa-pencil" aria-hidden="true"></i></button></center></a></td>
<td><a href="'. base_url().'admin/proman/delsubpro?proname='.$pid.'&subproname='.$p.'"><center><button type="button" class="btn btn-info btn-xs delpro" id="'. $p .'"><i class="fa fa-trash-o" style="color:red" aria-hidden="true"></i></button></center></a></td><tr>';
						}
					$resHTML.='</tbody></table>';
					echo $resHTML;
					}
				}
				else 
					echo "";
	}
	public function getEditpro(){
					$pid=$_REQUEST['proname'];
					$new_pid=$_REQUEST['new_proname'];
					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/editproject/?pid=".urlencode($pid)."&flag=1&new_pid=".urlencode($new_pid);
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);
					$rmsg = $res;
					if(empty($rmsg)){
					$resHTML='<br><span class="alert alert-danger">
  <strong>Please try again!';
					echo $resHTML;}
					else{
					
					echo '<br><span class="alert alert-info">
  <strong>Project updated successfully!';
					}
	}


	public function getEditsubpro(){
					$pid=$_REQUEST['proname'];
					$spid=$_REQUEST['subproname'];
					$new_spid=$_REQUEST['new_subproname'];
					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/editsubproject/?pid=".urlencode($pid)."&spid=".urlencode($spid)."&flag=1&new_spid=".urlencode($new_spid);
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);
					$rmsg = $res;
					if(empty($rmsg)){
					$resHTML='<br><span class="alert alert-danger">
  <strong>Please try again!';
					echo $resHTML;}
					else{
					
					echo '<br><span class="alert alert-info">
  <strong>Wave updated successfully!';
					}
	}


	public function getAddsubpro(){
		
			$query=$this->input->post('subpro');
			$pid=$this->input->post('proname');
					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/subprojectin/?pid=".$pid."&spid=".urlencode($query);
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);
					$rmsg = $res['Response Message'];
					if ( $rmsg=='Subproject Already exists')
					{
						$resHTML = '<div class="alert alert-warning">
  <strong>Wave already exists!</strong> Please try again with a different name.
</div>';
					}
					else if ($rmsg=='Subproject Inserted')
					{
						$resHTML = '<div class="alert alert-success">
  <strong>Wave added Succesfully!</strong>.
</div>';
					curl_close($curl);
					}
					else
					{
						$resHTML = '<div class="alert alert-danger">
  <strong>Something went wrong!</strong> Please contact support team.
</div>';
					}
					echo $resHTML;
	}




	public function getAddpro(){
		
			$query=$this->input->post('proname');
					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/projectin/?pid=".urlencode($query);
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);
					$rmsg = $res['Response Message'];
					if ( $rmsg=='Project Creation Success')
					{
					$resHTML = '<div class="alert alert-success">
  <strong>Project Created!</strong> Please add a Wave to continue.<br><a class="pull-right" href="http://tbcs.digitallycans.com/admin/proman/addsubpro"><button class="btn btn-success" type="submit" name="add_wave" id="add_wave" value="Add New Wave"><i class="fa fa-check-square-o"></i> Add New Wave</button></a><br><br>
</div>';
					}
					else
					{
						$resHTML = '<div class="alert alert-danger">
  <strong>Same Project exists!</strong> Please choose another name</div>';
					}
					echo $resHTML;
	}





public function getUploadfile()
{
	$config['upload_path']= HTTP_PATH."uploads";
    $config['allowed_types']='xls|xlsx';
    $this->load->library('upload',$config);
	if(!isset($_FILES['bulkfile']['name']))
	{
		echo '<script>alert("Sorry, no file found. Please select a file");</script>';
	}
	else
	{

		$pid=$this->input->post('proname');
		$spid=$this->input->post('subproname');
		$file_type=$_FILES['bulkfile']['type'];
		$file_size=$_FILES['bulkfile']['size'];
		$fname=urlencode($pid."-".$spid."-".strtolower($_FILES['bulkfile']['name']));
		$p=strpos($fname,".");
		$ext=substr($fname,$p+1,strlen($fname)-$p);
		if($file_size<=12614400000)
		{
			switch($ext)
			{
		
				case 'xls' : case 'xlsx':{ move_uploaded_file($_FILES['bulkfile']['tmp_name'],"bulkfile/$fname");
				
				$curl = curl_init();
				$url = "http://tbcs.digitallycans.com:8000/getbulk/?url=$fname&pid=$pid&spid=$spid";
				curl_setopt($curl,CURLOPT_URL,$url);
				curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
				$res=curl_exec($curl);
				$res=(array)json_decode($res);
				if (empty($res))
				{
					echo '<script>alert("Invalid File name or File contains more than 600 strings!");</script>';
				}
				
				else if($res['Response Data in file']== "No Match")
				{
					$resHTML = '<div class="alert alert-success">
					  <strong>No Match found in File!</strong></div>';
					echo $resHTML;

				}
				
				else if(!empty($res['Response Data in file']))
				{
					$res=$res['Response Data in file'];
					$resHidden='<div id="exptbl"><table><tr><th>S. No.</th><th>String</td><th>Matched With String</td><th>Percentage</td></tr>';
					$resHTML='<div><center><h2 class="success text-success">Matched Strings within File</h2></center><br><button class="btn btn-success pull-right" id="btnExport">Export Excel</button><br><div class="exptbl"><div><table class="table table-bordered table-hover"><tr><th>S. No.</th><th>String</td><th>Matched With String</td><th>Percentage</td></tr>';
					$i=1;
					foreach ($res as $data)
					{
						$temp = (array)$data;
						//echo $temp['percent']." - ". $temp['string']." - ". $temp['matched_with'] ."\n";
						if ($temp['percent']>75)
							$pbar = 'progress-bar-danger';
						else
							$pbar = 'progress-bar-warning';	
						$resHidden.= '<tr><td>'.$i.'</td><td>'.$temp['string'].'</td><td>'.$temp['matched_with'].'</td><td>'.$temp['percent'].'</td></tr>';

	
						$resHTML.='<tr><td>'.$i.'</td><td>'.$temp['string'].'</td><td>'.$temp['matched_with'].'</td><td><div class="progress"><div class="progress-bar '.$pbar.'" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="'. $temp['percent'].'" style="width:'. $temp['percent'].'%">'. floor($temp['percent']).'% Matched</div></div></td></tr>';
						$i++;
					}
					$resHTML.='</table><div></div></div>';
					$resHidden.='</table></div>';
					$resHTML.='<div class="notshow">'.$resHidden.'</div>';
					echo $resHTML;
				}
				else if($res['Response Message']== "No Match")
				{
					$resHTML = '<div class="alert alert-success">
					  <strong>No Match found!</strong> File Successfully inserted in Database.
					</div>';
					echo $resHTML;

				}
				else
				{
					echo '<script>alert("Something went wrong!! Please Contact Support Teams");</script>';
				}
				break;
			}
				default: echo '<script>alert("Please select Excel file of format XLS or XLSX only!");</script>';
		}
	}
	else echo '<script>alert("File too big!");</script>';
}
	//else echo "not uploaded";
        //}
	//else echo "error";

     }







	public function getSingleinsert(){
		
			$query=$this->input->post('string');
			$spid=$this->input->post('subproname');
			$db=$this->input->post('db');
			$pid=$_REQUEST['proname'];
					$curl = curl_init();
					$url= "http://tbcs.digitallycans.com:8000/insert/?pid=".$pid."&spid=".$spid."&query=".urlencode($query)."&db=".$db;
					curl_setopt($curl,CURLOPT_URL,$url);
					curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
					$res=curl_exec($curl);
					$res=(array)json_decode($res);
					$rmsg = $res['Response Message'];
					if ( $rmsg=='Insertion Success')
					{
						$resHTML = '<div class="alert alert-success">
  <strong>Inserted Succsfully!</strong></div>';
					}
					else if ($res['Response Code']=='500')
					{
					$this->get404();
					}
					else
					{
						$resHTML = '<div class="alert alert-danger">
  <strong>No string to match!</strong> Please input a String.
</div>';
					}
					echo $resHTML;
	}








	public function getBulkinsert(){

	$config['upload_path']= HTTP_PATH."uploads";
    $config['allowed_types']='xls|xlsx';
    $this->load->library('upload',$config);
	if(!isset($_FILES['bulkfile']['name']))
	{
		echo '<script>alert("Sorry, no file found. Please select a file");</script>';
	}
	else
	{

		$pid=$this->input->post('proname');
		$spid=$this->input->post('subproname');
		$file_type=$_FILES['bulkfile']['type'];
		$file_size=$_FILES['bulkfile']['size'];
		$fname=urlencode($pid."-".$spid."-".strtolower($_FILES['bulkfile']['name']));
		$p=strpos($fname,".");
		$ext=substr($fname,$p+1,strlen($fname)-$p);
		if($file_size<=12614400000)
		{
			switch($ext)
			{
		
				case 'xls' : case 'xlsx':{ move_uploaded_file($_FILES['bulkfile']['tmp_name'],"bulkfile/$fname");
				
				$curl = curl_init();
				$url = "http://tbcs.digitallycans.com:8000/dbgetbulk/?url=$fname&pid=$pid&spid=$spid";
				curl_setopt($curl,CURLOPT_URL,$url);
				curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
				$res=curl_exec($curl);
				$res=(array)json_decode($res);
				if (empty($res))
				{
					echo '<script>alert("File has more than 600 Strings!! Please use small file or Contact Support Teams");</script>';
				}
				else if(empty($res['Response Data']))
				{
					$resHTML = '<div class="alert alert-success">
					  <strong>No Match found in Database!</strong>File inserted successfully</div>';
					echo $resHTML;

				}				
				else if(!empty($res['Response Data']))
				{

					$res=$res['Response Data'];
					$resHidden='<div id="exptbl"><table><tr><th>S. No.</th><th>String</td><th>Matched With String</td><th>Percentage</td></tr>';
					$resHTML='<div><center><h2 class="success text-success">Matched Strings with Database</h2></center><br><button class="btn btn-success pull-right" id="btnExport">Export Excel</button><br><div class="exptbl"><div><table class="table table-bordered table-hover"> <tr><th>S. No.</th><th>String</td><th>Matched With String</td><th>Percentage</td></tr>';
					$i=1;
					foreach ($res as $data)
					{
						$temp = (array)$data;
						//print_r($temp);
						//echo $temp['percent']." - ". $temp['string']." - ". $temp['matched_with'] ."\n";
						foreach ($temp['matched'] as $data1)
						{
							$temp1=(array)$data1;
							//print_r($temp1);
							if ($temp1['perc']>75)
								$pbar = 'progress-bar-danger';
							else
								$pbar = 'progress-bar-warning';	
						$resHidden.= '<tr><td>'.$i.'</td><td>'.$temp['string'].'</td><td>'.$temp1['stri'].'</td><td>'.$temp1['perc'].'</td></tr>';
							$resHTML.='<tr><td>'.$i.'</td><td>'.$temp['string'].'</td><td>'.$temp1['stri'].'</td><td><div class="progress"><div class="progress-bar '.$pbar.'" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="'. $temp1['perc'].'" style="width:'. $temp1['perc'].'%">'. floor($temp1['perc']).'% Matched</div></div></td></tr>';
							$i++;
						}
					}

					$resHTML.='</table></div></div></div>';
					$resHidden.='</table></div>';
					$resHTML.='<div class="notshow">'.$resHidden.'</div>';
					echo $resHTML;
				}
				else if($res['Response Message']== "No Match")
				{
					$resHTML = '<div class="alert alert-success">
					  <strong>No Match found!</strong> File Successfully inserted in Database.
					</div>';
					echo $resHTML;

				}
				else
				{
					echo '<script>alert("Something went wrong!! Please Contact Support Teams");</script>';
				}
				break;
			}
				default: echo '<script>alert("Please select Excel file of format XLS or XLSX only!");</script>';;
		}
	}
	else echo '<script>alert("File too big!");</script>';
}
	}





    
	public function get404(){
		
		 		$data = '<script>alert("Something went wrong!! Please Contact Support Team");</script>';
				 echo $data;
	}
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
