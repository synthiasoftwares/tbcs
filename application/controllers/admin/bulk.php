<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bulk extends CI_Controller {
/**
 * ark Admin Panel for Codeigniter 
 * Author: Abhishek R. Kaushik
 * downloaded from http://devzone.co.in
 *
 */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
	else if ($this->session->userdata('user_type')=="AG"){ redirect('admin/check');}
    }

    public function index() {
        $arr['page'] = 'bulk';
        $this->load->view('admin/bulk',$arr);
    }

    
    
    
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
