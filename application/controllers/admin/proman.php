<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proman extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
	else if ($this->session->userdata('user_type')=="AG"){ redirect('admin/check');}
    }

    public function index() {
        $arr['page'] = 'proman';
        $this->load->view('admin/proman',$arr);
    }

    public function addsubpro() {
        $this->load->view('admin/addsp');
    }

    public function addpro() {
        $this->load->view('admin/proadd');
    }

    public function editpro() {
	$data['proname'] = $this->input->get('proname');
        $this->load->view('admin/editpro',$data);
    }
    public function editsubpro() {
	$data['proname'] = $this->input->get('proname');
	$data['subproname'] = $this->input->get('subproname');
        $this->load->view('admin/editsubpro',$data);
    }

    public function delpro() {
	$pid = $this->input->get('proname');
		$curl = curl_init();
		$url= "http://tbcs.digitallycans.com:8000/editproject/?pid=".urlencode($pid)."&flag=0";
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		$res=curl_exec($curl);
		$res=(array)json_decode($res);
        redirect('admin/proman');
    }

    public function delsubpro() {
	$pid = $this->input->get('proname');
	$spid = $this->input->get('subproname');
		$curl = curl_init();
		$url= "http://tbcs.digitallycans.com:8000/editsubproject/?pid=".urlencode($pid)."&spid=".urlencode($spid)."&flag=0";
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		$res=curl_exec($curl);
		$res=(array)json_decode($res);
        redirect('admin/proman');
    }
    
    
    
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/admin/proman.php */
