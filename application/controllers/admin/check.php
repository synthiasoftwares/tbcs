<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Check extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
         if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    public function index() {
        $arr['page'] = 'check';
        $this->load->view('admin/check',$arr);
    }

    
    
    
    

}

/* End of file welcome.php */
/* Location: ./application/controllers/admin/check.php */
