<div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
        Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
        Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    TBCS
                </a>
            </div>

            <ul class="nav"><?php if ($this->session->userdata('user_type')=="SA"){ ?>
                <!--<li class="active">
                    <a href="<?php echo base_url(); ?>admin/dashboard">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>-->
                <li>
                    <a href="<?php echo base_url(); ?>admin/proman">
                        <i class="ti-user"></i>
                        <p>Project Management</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>admin/bulk">
                        <i class="ti-view-list-alt"></i>
                        <p> Bulk Response Check</p>
                    </a>
                </li><?php } ?>
                <li <?php if ($this->session->userdata('user_type')=="AG"){ echo ' class="active"';} ?>>
                    <a href="<?php echo base_url(); ?>admin/check">
                        <i class="ti-text"></i>
                        <p>Single Response Check</p>
                    </a>
                </li>


                <li class="active-pro">
                    <a href="<?php echo base_url(); ?>admin/home/logout">
                        <i class="ti-export"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

<!--header nav bar -->


    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                       
						<li>
                            <a href="<?php echo base_url(); ?>admin/home/logout">
								<i class="ti-export"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
